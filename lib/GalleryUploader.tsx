import { ChangeEvent, useEffect, useMemo, useRef, useState } from 'react';
import { GalleryUploaderProps, ImageData } from './types';
import GalleryUploaderCore from './GalleryUploaderCore';
import GalleryUploaderContext from './GalleryUploaderContext';
import ImageContainer from './image-container/ImageContainer';

import './gallery-uploader.css';

const GalleryUploader = ({
	uploadUrl,
	updateUrl,
	deleteUrl,
	exchangePositionUrl,
	customRequestInit,
	images = [],
}: GalleryUploaderProps) => {
	const galleryUploaderRef = useRef(null);
	let [imagesList, setImagesList] = useState<ImageData[]>([]);
	let [updater, setUpdater] = useState(0);
	let core = useMemo(() => {
		return new GalleryUploaderCore();
	}, []);

	useEffect(() => {
		const update = () => {
			updater = updater + 1;
			setUpdater(updater);
		};

		core.updateImages = (imagesData: ImageData[]) => {
			setImagesList(imagesData);
			//setImages([...imagesData]);
			update();
		};
	}, [core, updater]);

	useEffect(() => {
		if (images.length > 0) {
			const imagesList: ImageData[] = images.map((img) => {
				return { ...img, status: 'success', subStatus: 'uploaded' };
			});
			core.imagesList = imagesList;
		}
	}, [core, images]);

	useEffect(() => {
		let des = () => {};

		if (galleryUploaderRef.current) {
			core.galleryUploader = galleryUploaderRef.current;
			core.sendUrls = {
				uploadUrl: uploadUrl,
				updateUrl: updateUrl,
				deleteUrl: deleteUrl,
				getUrl: '',
				exchangePositionUrl: exchangePositionUrl,
			};
			if (customRequestInit) {
				core.getCustomRequestInit = customRequestInit;
			}

			setImagesList(core.imagesList);

			des = core.destroy.bind(core);
		}

		return des;
	}, [core, uploadUrl, updateUrl, deleteUrl, exchangePositionUrl, customRequestInit]);

	return (
		<GalleryUploaderContext.Provider value={{ core }}>
			<div ref={galleryUploaderRef} className="gallery-uploader">
				{imagesList.map((image, i) => (
					<ImageContainer
						key={i}
						id={image.id}
						src={image.src}
						name={image.name}
						fileName={image.fileName}
						featured={image.featured}
						position={i}
						status={image.status}
						subStatus={image.subStatus ?? ''}
					/>
				))}

				<label className="file-selector">
					Select file
					<input
						type="file"
						onChange={(event: ChangeEvent) => {
							core.onFileSelectorChange(event);
						}}
					/>
				</label>
			</div>
		</GalleryUploaderContext.Provider>
	);
};

export default GalleryUploader;
