import { createContext } from 'react';
import GalleryUploaderCore from './GalleryUploaderCore';

const GalleryUploaderContext = createContext({
	core: new GalleryUploaderCore(),
});

export default GalleryUploaderContext;
