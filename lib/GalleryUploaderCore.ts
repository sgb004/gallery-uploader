import {
	ImageMessage,
	ImageInfo,
	ImageData,
	ImageStatus,
	ImageSubStatus,
	ImageSend,
	ImageMessageList,
	CustomRequestInit,
} from './types';

class GalleryUploaderCore {
	#base: HTMLElement;
	updateImages: (imagesData: ImageData[]) => void;
	imagesList: ImageData[] = [];
	messagesList: ImageMessageList = {};
	sendUrls = { updateUrl: '', uploadUrl: '', deleteUrl: '', getUrl: '', exchangePositionUrl: '' };
	getCustomRequestInit: CustomRequestInit;

	constructor(imagesList?: ImageData[]) {
		console.log('CONSTRUCTOR GalleryUploaderCore');

		if (imagesList) {
			this.imagesList = imagesList;
		}

		this.#base = document.createElement('div');

		this.updateImages = () => {};
	}

	destroy() {
		console.log('DESTRUCTOR GalleryUploaderCore');
		this.removeListeners();
	}

	removeListeners() {}

	set galleryUploader(galleryUploader: HTMLElement) {
		this.removeListeners();
		this.#base = galleryUploader;
	}

	showMessage(id: string, message: ImageMessage) {
		this.messagesList[id] = message;

		if (message.type === 'success') {
			console.log(message.message);
		} else if (message.type === 'error') {
			console.error(message.message);
		} else {
			console.warn(message.message);
		}
	}

	getPositionImage(id: string) {
		return this.imagesList.findIndex((image) => image.id === id);
	}

	getImageData(id: string) {
		const position = this.getPositionImage(id);
		return this.imagesList[position];
	}

	changeStatusImage(id: string, status: ImageStatus, subStatus?: ImageSubStatus) {
		const position = this.getPositionImage(id);
		this.imagesList[position].status = status;
		this.imagesList[position].subStatus = subStatus;
		this.reloadImages();
	}

	reloadImages() {
		this.updateImages(this.imagesList);
	}

	cleanName(fileName: string) {
		let name = fileName;
		name = name.replace(/[\-\_]/gm, ' ');
		name = name.replace(/\.[^/.]+$/, '');
		return name.trim();
	}

	handleFiles(files: FileList) {
		let imagesInFiles = [];

		for (let i = 0; i < files.length; i++) {
			imagesInFiles.push(this.getImageFromFile(files[i]));
		}

		Promise.all(imagesInFiles)
			.then((imagesInfo) => {
				for (const imageInfo of imagesInfo) {
					const uid = new Date().getTime();
					const image: ImageData = {
						id: `${uid}`,
						status: 'success',
						featured: false,
						...imageInfo,
					};

					this.imagesList.push(image);
					this.reloadImages();
					this.uploadImage(image);
				}
			})
			.catch((message) =>
				this.showMessage('none', {
					type: 'error',
					message,
				})
			);
	}

	getImageFromFile(file: File) {
		return new Promise<ImageInfo>((resolve, reject) => {
			if (file.type.startsWith('image/')) {
				const reader = new FileReader();

				reader.onload = (e) => {
					if (e.target && typeof e.target.result == 'string') {
						resolve({
							src: e.target.result,
							name: this.cleanName(file.name),
							fileName: file.name,
						});
					}
				};
				reader.readAsDataURL(file);
			} else {
				reject(`The file "${file.name}" is not an image.`);
			}
		});
	}

	getRequestInit(config: ImageSend) {
		let headers = {
			'Content-Type': 'application/json',
			Accept: 'application/json, text-plain, */*',
		};

		let body: { [key: string]: any } = config.body;
		const requestInit = this.getCustomRequestInit ? this.getCustomRequestInit(config.type) : {};

		this.changeStatusImage(config.id, 'warning', 'sending');

		headers =
			typeof requestInit.headers == 'object'
				? { ...requestInit.headers, ...headers }
				: headers;
		body = typeof requestInit.body == 'object' ? { ...requestInit.body, ...body } : body;
		body['id'] = config.id;

		requestInit.headers = headers;
		requestInit.method = 'POST';
		requestInit.body = JSON.stringify(body);

		return requestInit;
	}

	send(config: ImageSend) {
		const requestInit = this.getRequestInit(config);

		fetch(config.url, requestInit)
			.then((data) => data.json())
			.then((data) => {
				if (config.afterFetch) config.afterFetch(data);

				if (data.success === true) {
					this.changeStatusImage(config.id, 'success', config.status.after);

					this.showMessage(config.id, {
						type: 'success',
						subtype: config.status.after,
						message: data.message,
					});
				} else {
					this.showMessage(config.id, {
						type: 'error',
						subtype: config.error.type,
						message: data.message,
					});

					this.changeStatusImage(config.id, 'error', config.error.type);
				}

				if (config.afterFetchComplete) {
					config.afterFetchComplete(data);
				}
			})
			.catch((error) => {
				this.showMessage(config.id, {
					type: 'error',
					subtype: config.error.type,
					message: config.error.message,
				});

				this.changeStatusImage(config.id, 'error', config.error.type);
				if (config.afterFetchError) config.afterFetchError();
				throw new Error(error);
			});
	}

	uploadImageChanged(id: string, file: File) {
		this.getImageFromFile(file)
			.then((imageInfo) => {
				const position = this.getPositionImage(id);

				const image: ImageData = {
					id: `${id}`,
					status: 'success',
					featured: this.imagesList[position].featured,
					...imageInfo,
				};

				this.imagesList[position].src = imageInfo.src;
				this.uploadImage(image);
			})
			.catch((message) =>
				this.showMessage(id, {
					type: 'error',
					subtype: '',
					message,
				})
			);
	}

	uploadImage(imageData: ImageData) {
		this.send({
			id: imageData.id,
			type: 'upload',
			status: {
				after: 'uploaded',
			},
			error: {
				type: 'upload',
				message: 'Error to upload.',
			},
			requestInit: 'uploadRequestInit',
			url: this.sendUrls.uploadUrl,
			body: imageData,
		});
	}

	updateImage(
		id: string,
		name: string,
		fileName: string,
		featured: boolean,
		errorMessage: string,
		afterFetchError?: () => void,
		afterFetchComplete?: () => void
	) {
		this.send({
			id,
			type: 'update',
			status: {
				after: 'updated',
			},
			error: {
				type: 'update',
				message: errorMessage,
			},
			requestInit: 'updateRequestInit',
			url: this.sendUrls.updateUrl,
			body: {
				name,
				fileName,
				featured,
			},
			afterFetch: (data) => {
				if (data.success !== true && afterFetchError) {
					afterFetchError();
				}
			},
			afterFetchError,
			afterFetchComplete,
		});
	}

	resendUpload(id: string) {
		this.uploadImage(this.getImageData(id));
	}

	changeImageNames(id: string, name: string, fileName: string) {
		const position = this.getPositionImage(id);

		this.updateImage(
			id,
			name,
			fileName,
			this.imagesList[position].featured,
			'Error to change the info of the image.',
			() => {},
			() => {
				this.imagesList[position].name = name;
				this.imagesList[position].fileName = fileName;
				this.reloadImages();
			}
		);
	}

	exchangePosition(origin: string, destiny: string) {
		const ori = parseInt(origin);
		const des = parseInt(destiny);
		const originId = this.imagesList[ori].id;
		const destinyId = this.imagesList[des].id;
		const image = this.imagesList[des];

		this.imagesList[des] = this.imagesList[ori];
		this.imagesList[ori] = image;

		this.send({
			id: originId,
			type: 'exchange',
			status: {
				after: 'exchanged',
			},
			error: {
				type: 'exchange',
				message: 'Error to change position',
			},
			requestInit: 'updateRequestInit',
			url: this.sendUrls.exchangePositionUrl,
			body: {
				origin,
				destiny,
				originId,
				destinyId,
			},
		});
	}

	onFileSelectorChange(event: React.ChangeEvent) {
		const inputFile = event.target;
		if (inputFile instanceof HTMLInputElement && inputFile.files) {
			this.handleFiles(inputFile.files);
			inputFile.value = '';
		}
	}

	onUploadImageChange(id: string, event: React.ChangeEvent) {
		const inputFile = event.target;
		if (inputFile instanceof HTMLInputElement && inputFile.files) {
			this.uploadImageChanged(id, inputFile.files[0]);
			inputFile.value = '';
		}
	}

	onFeaturedChange(id: string, event: React.ChangeEvent) {
		const input = event.target;

		if (input instanceof HTMLInputElement) {
			const position = this.getPositionImage(id);

			const featured = input.checked;
			const prevFeatured = this.imagesList[position].featured;
			const afterFetchError = () => {
				this.imagesList[position].featured = prevFeatured;
				input.checked = prevFeatured;
			};

			this.imagesList[position].featured = featured;
			this.updateImage(
				id,
				this.imagesList[position].name,
				this.imagesList[position].fileName,
				featured,
				'Error to change the value of featured of the image.',
				afterFetchError
			);
		}
	}

	onDelete(id: string) {
		const result = window.confirm(
			'Do you want to delete the image? This action cannot be undone.'
		);

		if (result) {
			this.send({
				id,
				type: 'delete',
				status: {
					after: 'deleted',
				},
				error: {
					type: 'delete',
					message: 'Error to delete the image.',
				},
				requestInit: 'updateRequestInit',
				url: this.sendUrls.deleteUrl,
				body: {},
				afterFetchComplete: (data) => {
					if (data.success === true) {
						this.imagesList = this.imagesList.filter(
							(imageData) => imageData.id !== id
						);
						this.reloadImages();
					}
				},
			});
		}
	}

	onStatusClick(id: string) {
		if (this.messagesList[id]) {
			this.showMessage(id, this.messagesList[id]);
		}
	}

	onDownload(id: string) {
		const image = this.getImageData(id);
		const [baseType, base64] = image.src.split(',');
		const type = baseType.replace(/(data:)|(;base64)/g, '');
		const decodedBase64 = atob(base64);
		const arrayBuffer = new ArrayBuffer(decodedBase64.length);
		const bytes = new Uint8Array(arrayBuffer);
		for (let i = 0; i < decodedBase64.length; i++) {
			bytes[i] = decodedBase64.charCodeAt(i);
		}
		const blob = new Blob([bytes], { type });
		const url = window.URL.createObjectURL(blob);
		const a = document.createElement('a');

		a.style.display = 'none';
		a.href = url;
		a.download = image.fileName;
		this.#base.appendChild(a);
		a.click();

		window.URL.revokeObjectURL(url);
		this.#base.removeChild(a);
	}
}

export default GalleryUploaderCore;
