import { useEffect, useMemo, useRef, useContext, useState } from 'react';
import { ImageContainerProps, ImageNames } from '../types';
import GalleryUploaderContext from '../GalleryUploaderContext';
import ImageContainerCore from './ImageContainerCore';
import Featured from './Featured';
import Delete from './Delete';
import InfoEditor from './InfoEditor';
import ResendUpload from './ResendUpload';
import Upload from './Upload';
import Download from './Download';

const ImageContainer = ({
	id,
	src,
	name,
	fileName,
	featured,
	position,
	status,
	subStatus = '',
}: ImageContainerProps) => {
	const galleryUploaderContext = useContext(GalleryUploaderContext);
	const guCore = galleryUploaderContext.core;
	const imageContainerRef = useRef<HTMLDivElement>(null);
	let core = useMemo(() => new ImageContainerCore(), []);
	let [cssClasses, setCSSClasses] = useState({ classesNames: '', typeUploadBtn: '' });

	useEffect(() => {
		let classesNames = `${status} ${subStatus}`;
		let typeUploadBtn = cssClasses.typeUploadBtn;

		if (status === 'error' && subStatus === 'upload') {
			typeUploadBtn = 'show-resend-upload-btn';
		} else if (status === 'success' && subStatus === 'uploaded') {
			typeUploadBtn = 'show-upload-btn';
		}

		if (
			cssClasses.classesNames !== classesNames ||
			cssClasses.typeUploadBtn !== typeUploadBtn
		) {
			setCSSClasses({ classesNames, typeUploadBtn });
		}
	}, [core, status, subStatus, cssClasses]);

	useEffect(() => {
		console.log({ core });

		let des = () => {};

		if (imageContainerRef.current) {
			core.exchangePosition = guCore.exchangePosition.bind(guCore);
			core.imageContainer = imageContainerRef.current;

			des = core.destroy.bind(core);
		}

		return des;
	}, [core, guCore]);

	return (
		<div
			ref={imageContainerRef}
			className={`image-container ${cssClasses.classesNames} ${cssClasses.typeUploadBtn}`}
			draggable="true"
			data-position={position}
			data-id={id}
		>
			<img className="media" alt="" src={src} />
			<div className="header">
				<Featured
					featured={featured}
					onChange={(event) => {
						guCore.onFeaturedChange(id, event);
					}}
				/>
				<Delete
					onDelete={() => {
						guCore.onDelete(id);
					}}
				/>
			</div>
			<div className="footer">
				<button
					className="status button"
					onClick={() => {
						guCore.onStatusClick(id);
					}}
				></button>
				<Download
					onClick={() => {
						guCore.onDownload(id);
					}}
					url={src}
					fileName={fileName}
				/>
				<ResendUpload
					onClick={() => {
						guCore.resendUpload(id);
					}}
				/>
				<Upload
					onChange={(event) => {
						guCore.onUploadImageChange(id, event);
					}}
				/>
				<InfoEditor
					id={id}
					info={{ name, fileName }}
					setInfo={(info: ImageNames) => {
						guCore.changeImageNames(id, info.name, info.fileName);
					}}
				/>
			</div>
		</div>
	);
};

export default ImageContainer;
