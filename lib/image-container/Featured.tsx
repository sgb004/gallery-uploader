import { FeaturedProps } from '../types';

const Featured = ({ featured, onChange }: FeaturedProps) => (
	<div className="featured button">
		<input
			className="dispatcher"
			type="checkbox"
			onChange={onChange}
			defaultChecked={featured}
		/>
		<svg
			className="icon"
			width="20"
			height="30"
			xmlns="http://www.w3.org/2000/svg"
			viewBox="0 0 20 30"
		>
			<path
				clipRule="evenodd"
				d="M.5 2.649c0-1.187.945-2.15 2.111-2.15h14.778c1.166 0 2.111.963 2.111 2.15v24.713c0 1.77-1.986 2.781-3.378 1.719L10 24.407 3.878 29.08C2.486 30.143.5 29.132.5 27.36z"
				fill="transparent"
				fillRule="evenodd"
				stroke="#000"
			/>
		</svg>
	</div>
);

export default Featured;
