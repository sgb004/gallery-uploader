import { DragDropItem, ExchangePosition } from '@wudev/drag-drop';

class ImageContainerCore {
	#base: HTMLElement;
	#dragDrop: DragDropItem | null = null;
	observer: MutationObserver;
	exchangePosition: ExchangePosition = () => {};

	constructor() {
		console.log('CONSTRUCTOR ImageContainerCore');

		this.#base = document.createElement('div');

		this.observer = new MutationObserver(() => {});

		this.observer.observe(this.#base, {
			attributes: true,
			attributeFilter: ['class'],
		});
	}

	destroy() {
		console.log('DESTRUYENDO ImageContainerCore');
		this.removeListeners();
	}

	removeListeners() {
		if (this.#dragDrop) {
			this.#dragDrop.destroy();
		}

		this.observer.disconnect();
	}

	set imageContainer(imageContainer: HTMLElement) {
		this.removeListeners();

		const observerCallback = () => {
			if (this.#base.classList.contains('sending')) {
				this.disableInputs();
			} else if (
				this.#base.classList.contains('error') &&
				this.#base.classList.contains('upload')
			) {
				this.enableInputs('.delete, .download, .resend-upload');
			} else {
				this.enableInputs();
			}
		};

		this.#base = imageContainer;

		this.observer = new MutationObserver(observerCallback);
		this.observer.observe(this.#base, {
			attributes: true,
			attributeFilter: ['class'],
		});

		this.#dragDrop = new DragDropItem(this.#base, '.gallery-uploader', this.exchangePosition);
	}

	get imageContainer() {
		return this.#base;
	}

	disableInputs(selectors = 'input:not(.status), button:not(.status)') {
		const inputs = this.#base.querySelectorAll(selectors);

		for (let i = 0; i < inputs.length; i++) {
			inputs[i].setAttribute('disabled', 'disabled');
		}
	}

	enableInputs(selectors = 'input:not(.status), button:not(.status)') {
		const inputs = this.#base.querySelectorAll(selectors);

		for (let i = 0; i < inputs.length; i++) {
			inputs[i].removeAttribute('disabled');
		}
	}
}

export default ImageContainerCore;
