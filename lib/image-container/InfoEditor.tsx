import { useRef } from 'react';
import { InfoEditorProps } from '../types';

const InfoEditor = ({ id, info, setInfo }: InfoEditorProps) => {
	const nameInputRef = useRef<HTMLInputElement>(null);
	const fileNameInputRef = useRef<HTMLInputElement>(null);
	const disId = `info-editor-dis-${id}`;

	const onAcceptButtonClick = () => {
		if (nameInputRef.current && fileNameInputRef.current) {
			setInfo({
				name: nameInputRef.current.value,
				fileName: fileNameInputRef.current.value,
			});
		}
	};

	const onCancelButtonClick = () => {
		if (nameInputRef.current && fileNameInputRef.current) {
			nameInputRef.current.value = info.name;
			fileNameInputRef.current.value = info.fileName;
		}
	};

	return (
		<div className="info-editor">
			<input
				id={disId}
				className="dispatcher"
				type="checkbox"
				onClick={onCancelButtonClick}
			/>
			<label className="button" htmlFor={disId}>
				<svg
					className="icon"
					height="30"
					width="30"
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 30 30"
				>
					<path
						d="M23.28.5l6.22 6.22-4.742 4.743-6.22-6.22zM.5 29.5h6.22l15.107-15.108-6.22-6.22L.5 23.28z"
						fill="none"
						stroke="#000"
						strokeLinecap="round"
						strokeLinejoin="round"
					/>
				</svg>
			</label>
			<div className="info-editor-form">
				<div className="row">
					<label htmlFor={`${disId}-file-name`}>Name:</label>
					<input
						ref={nameInputRef}
						id={`${disId}-file-name`}
						name="name"
						defaultValue={info.name}
					/>
				</div>
				<div className="row">
					<label htmlFor={`${disId}-name`}>File name:</label>
					<input
						ref={fileNameInputRef}
						id={`${disId}-name`}
						name="file-name"
						defaultValue={info.fileName}
					/>
				</div>
				<div className="row actions">
					<button className="button accept" type="button" onClick={onAcceptButtonClick}>
						<svg
							className="icon"
							width="30"
							height="30"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 30 30"
						>
							<path
								d="M15.012.5A14.487 14.487 0 1029.5 14.988 14.487 14.487 0 0015.012.5zm7.517 11.468l-9.22 9.219a1.317 1.317 0 01-1.862 0l-3.95-3.951a1.317 1.317 0 011.861-1.863l3.02 3.02 8.288-8.288a1.317 1.317 0 011.863 1.863z"
								fill="transparent"
								stroke="#000"
								strokeLinejoin="round"
							/>
						</svg>
					</button>
					<label className="button cancel" htmlFor={disId}>
						<svg
							className="icon"
							width="30"
							height="30"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 30 30"
						>
							<path
								d="M15.012.5A14.487 14.487 0 1029.5 14.988 14.487 14.487 0 0015.012.5zm6.2 18.825a1.317 1.317 0 11-1.863 1.862l-4.337-4.337-4.337 4.337a1.317 1.317 0 11-1.862-1.862l4.337-4.337-4.337-4.337a1.317 1.317 0 111.862-1.863l4.337 4.337 4.337-4.337a1.317 1.317 0 111.863 1.863l-4.337 4.337z"
								fill="none"
								stroke="#000"
								strokeLinejoin="round"
							/>
						</svg>
					</label>
				</div>
			</div>
		</div>
	);
};

export default InfoEditor;
