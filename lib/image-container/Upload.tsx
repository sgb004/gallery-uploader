import { UploadProps } from '../types';

const Upload = ({ onChange }: UploadProps) => (
	<label className="upload button">
		<input className="dispatcher" type="file" onChange={onChange} />
		<svg
			className="icon"
			width="30"
			height="30"
			xmlns="http://www.w3.org/2000/svg"
			viewBox="0 0 30 30"
		>
			<path
				d="M11.31 7.341l2.079-1.884v16.797a1.611 1.45 0 003.222 0V5.457l2.078 1.884a1.611 1.45 0 002.288 0 1.611 1.45 0 000-2.058L16.144.935a1.611 1.45 0 00-2.288 0L9.023 5.283a1.618 1.455 0 002.288 2.058zm13.357 3.319h-3.223a1.611 1.45 0 000 2.898h3.223a1.611 1.45 0 011.61 1.45v10.144a1.611 1.45 0 01-1.61 1.45H5.333a1.611 1.45 0 01-1.61-1.45V15.008a1.611 1.45 0 011.61-1.45h3.223a1.611 1.45 0 000-2.898H5.333A4.833 4.348 0 00.5 15.008v10.144A4.833 4.348 0 005.333 29.5h19.334a4.833 4.348 0 004.833-4.348V15.008a4.833 4.348 0 00-4.833-4.348z"
				fill="none"
				stroke="#000"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	</label>
);

export default Upload;
