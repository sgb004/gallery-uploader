import GalleryUploader from '../lib/main';

const Examples = () => {
	return (
		<div id="examples">
			<GalleryUploader
				uploadUrl="http://localhost:9033/upload-success"
				updateUrl="http://localhost:9033/update-success"
				deleteUrl="http://localhost:9033/delete-success"
				exchangePositionUrl="http://localhost:9033/exchange-position-success"
				images={[
					{
						id: '1',
						src: 'http://labpru.com/imgs/10_harvesttime.jpg',
						name: '',
						fileName: '',
						featured: true,
					},
				]}
			/>
		</div>
	);
};

export default Examples;
