import { GalleryUploaderProps } from './types';
import './gallery-uploader.css';
declare const GalleryUploader: ({ uploadUrl, updateUrl, deleteUrl, exchangePositionUrl, customRequestInit, images, }: GalleryUploaderProps) => import("react/jsx-runtime").JSX.Element;
export default GalleryUploader;
