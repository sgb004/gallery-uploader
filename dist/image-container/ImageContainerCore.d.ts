import { ExchangePosition } from '@wudev/drag-drop';
declare class ImageContainerCore {
    #private;
    observer: MutationObserver;
    exchangePosition: ExchangePosition;
    constructor();
    destroy(): void;
    removeListeners(): void;
    set imageContainer(imageContainer: HTMLElement);
    get imageContainer(): HTMLElement;
    disableInputs(selectors?: string): void;
    enableInputs(selectors?: string): void;
}
export default ImageContainerCore;
