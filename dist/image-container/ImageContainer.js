import { jsxs as d, jsx as n } from "react/jsx-runtime";
import { useContext as U, useRef as N, useMemo as x, useState as y, useEffect as f } from "react";
import I from "../GalleryUploaderContext.js";
import w from "./ImageContainerCore.js";
import B from "./Featured.js";
import D from "./Delete.js";
import b from "./InfoEditor.js";
import k from "./ResendUpload.js";
import $ from "./Upload.js";
import v from "./Download.js";
const H = ({
  id: a,
  src: p,
  name: g,
  fileName: c,
  featured: C,
  position: u,
  status: l,
  subStatus: s = ""
}) => {
  const o = U(I).core, i = N(null);
  let t = x(() => new w(), []), [r, h] = y({ classesNames: "", typeUploadBtn: "" });
  return f(() => {
    let e = `${l} ${s}`, m = r.typeUploadBtn;
    l === "error" && s === "upload" ? m = "show-resend-upload-btn" : l === "success" && s === "uploaded" && (m = "show-upload-btn"), (r.classesNames !== e || r.typeUploadBtn !== m) && h({ classesNames: e, typeUploadBtn: m });
  }, [t, l, s, r]), f(() => {
    console.log({ core: t });
    let e = () => {
    };
    return i.current && (t.exchangePosition = o.exchangePosition.bind(o), t.imageContainer = i.current, e = t.destroy.bind(t)), e;
  }, [t, o]), /* @__PURE__ */ d(
    "div",
    {
      ref: i,
      className: `image-container ${r.classesNames} ${r.typeUploadBtn}`,
      draggable: "true",
      "data-position": u,
      "data-id": a,
      children: [
        /* @__PURE__ */ n("img", { className: "media", alt: "", src: p }),
        /* @__PURE__ */ d("div", { className: "header", children: [
          /* @__PURE__ */ n(
            B,
            {
              featured: C,
              onChange: (e) => {
                o.onFeaturedChange(a, e);
              }
            }
          ),
          /* @__PURE__ */ n(
            D,
            {
              onDelete: () => {
                o.onDelete(a);
              }
            }
          )
        ] }),
        /* @__PURE__ */ d("div", { className: "footer", children: [
          /* @__PURE__ */ n(
            "button",
            {
              className: "status button",
              onClick: () => {
                o.onStatusClick(a);
              }
            }
          ),
          /* @__PURE__ */ n(
            v,
            {
              onClick: () => {
                o.onDownload(a);
              },
              url: p,
              fileName: c
            }
          ),
          /* @__PURE__ */ n(
            k,
            {
              onClick: () => {
                o.resendUpload(a);
              }
            }
          ),
          /* @__PURE__ */ n(
            $,
            {
              onChange: (e) => {
                o.onUploadImageChange(a, e);
              }
            }
          ),
          /* @__PURE__ */ n(
            b,
            {
              id: a,
              info: { name: g, fileName: c },
              setInfo: (e) => {
                o.changeImageNames(a, e.name, e.fileName);
              }
            }
          )
        ] })
      ]
    }
  );
};
export {
  H as default
};
