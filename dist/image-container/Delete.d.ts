import { DeleteProps } from '../types';
declare const Delete: ({ onDelete }: DeleteProps) => import("react/jsx-runtime").JSX.Element;
export default Delete;
