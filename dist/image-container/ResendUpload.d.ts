import { ResendUploadProps } from '../types';
declare const ResendUpload: ({ onClick }: ResendUploadProps) => import("react/jsx-runtime").JSX.Element;
export default ResendUpload;
