import { jsxs as a, jsx as e } from "react/jsx-runtime";
import { useRef as r } from "react";
const h = ({ id: o, info: i, setInfo: c }) => {
  const l = r(null), t = r(null), n = `info-editor-dis-${o}`, s = () => {
    l.current && t.current && c({
      name: l.current.value,
      fileName: t.current.value
    });
  };
  return /* @__PURE__ */ a("div", { className: "info-editor", children: [
    /* @__PURE__ */ e(
      "input",
      {
        id: n,
        className: "dispatcher",
        type: "checkbox",
        onClick: () => {
          l.current && t.current && (l.current.value = i.name, t.current.value = i.fileName);
        }
      }
    ),
    /* @__PURE__ */ e("label", { className: "button", htmlFor: n, children: /* @__PURE__ */ e(
      "svg",
      {
        className: "icon",
        height: "30",
        width: "30",
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 30 30",
        children: /* @__PURE__ */ e(
          "path",
          {
            d: "M23.28.5l6.22 6.22-4.742 4.743-6.22-6.22zM.5 29.5h6.22l15.107-15.108-6.22-6.22L.5 23.28z",
            fill: "none",
            stroke: "#000",
            strokeLinecap: "round",
            strokeLinejoin: "round"
          }
        )
      }
    ) }),
    /* @__PURE__ */ a("div", { className: "info-editor-form", children: [
      /* @__PURE__ */ a("div", { className: "row", children: [
        /* @__PURE__ */ e("label", { htmlFor: `${n}-file-name`, children: "Name:" }),
        /* @__PURE__ */ e(
          "input",
          {
            ref: l,
            id: `${n}-file-name`,
            name: "name",
            defaultValue: i.name
          }
        )
      ] }),
      /* @__PURE__ */ a("div", { className: "row", children: [
        /* @__PURE__ */ e("label", { htmlFor: `${n}-name`, children: "File name:" }),
        /* @__PURE__ */ e(
          "input",
          {
            ref: t,
            id: `${n}-name`,
            name: "file-name",
            defaultValue: i.fileName
          }
        )
      ] }),
      /* @__PURE__ */ a("div", { className: "row actions", children: [
        /* @__PURE__ */ e("button", { className: "button accept", type: "button", onClick: s, children: /* @__PURE__ */ e(
          "svg",
          {
            className: "icon",
            width: "30",
            height: "30",
            xmlns: "http://www.w3.org/2000/svg",
            viewBox: "0 0 30 30",
            children: /* @__PURE__ */ e(
              "path",
              {
                d: "M15.012.5A14.487 14.487 0 1029.5 14.988 14.487 14.487 0 0015.012.5zm7.517 11.468l-9.22 9.219a1.317 1.317 0 01-1.862 0l-3.95-3.951a1.317 1.317 0 011.861-1.863l3.02 3.02 8.288-8.288a1.317 1.317 0 011.863 1.863z",
                fill: "transparent",
                stroke: "#000",
                strokeLinejoin: "round"
              }
            )
          }
        ) }),
        /* @__PURE__ */ e("label", { className: "button cancel", htmlFor: n, children: /* @__PURE__ */ e(
          "svg",
          {
            className: "icon",
            width: "30",
            height: "30",
            xmlns: "http://www.w3.org/2000/svg",
            viewBox: "0 0 30 30",
            children: /* @__PURE__ */ e(
              "path",
              {
                d: "M15.012.5A14.487 14.487 0 1029.5 14.988 14.487 14.487 0 0015.012.5zm6.2 18.825a1.317 1.317 0 11-1.863 1.862l-4.337-4.337-4.337 4.337a1.317 1.317 0 11-1.862-1.862l4.337-4.337-4.337-4.337a1.317 1.317 0 111.862-1.863l4.337 4.337 4.337-4.337a1.317 1.317 0 111.863 1.863l-4.337 4.337z",
                fill: "none",
                stroke: "#000",
                strokeLinejoin: "round"
              }
            )
          }
        ) })
      ] })
    ] })
  ] });
};
export {
  h as default
};
