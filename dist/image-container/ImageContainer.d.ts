import { ImageContainerProps } from '../types';
declare const ImageContainer: ({ id, src, name, fileName, featured, position, status, subStatus, }: ImageContainerProps) => import("react/jsx-runtime").JSX.Element;
export default ImageContainer;
