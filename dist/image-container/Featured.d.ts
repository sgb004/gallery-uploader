import { FeaturedProps } from '../types';
declare const Featured: ({ featured, onChange }: FeaturedProps) => import("react/jsx-runtime").JSX.Element;
export default Featured;
