import { UploadProps } from '../types';
declare const Upload: ({ onChange }: UploadProps) => import("react/jsx-runtime").JSX.Element;
export default Upload;
