import { DownloadProps } from '../types';
declare const Download: ({ onClick, url, fileName }: DownloadProps) => import("react/jsx-runtime").JSX.Element;
export default Download;
