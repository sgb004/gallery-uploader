import { InfoEditorProps } from '../types';
declare const InfoEditor: ({ id, info, setInfo }: InfoEditorProps) => import("react/jsx-runtime").JSX.Element;
export default InfoEditor;
