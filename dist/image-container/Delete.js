import { jsx as l } from "react/jsx-runtime";
const e = ({ onDelete: t }) => /* @__PURE__ */ l("button", { className: "delete button", onClick: t, children: /* @__PURE__ */ l(
  "svg",
  {
    className: "icon",
    width: "30",
    height: "30",
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 30 30",
    children: /* @__PURE__ */ l(
      "path",
      {
        d: "M20.441 15l8.164-8.162a3.09 3.09 0 000-4.353l-1.09-1.088a3.084 3.084 0 00-4.351 0L15 9.56 6.836 1.396a3.086 3.086 0 00-4.352 0L1.395 2.484a3.09 3.09 0 000 4.353L9.558 15l-8.163 8.164a3.088 3.088 0 000 4.352l1.09 1.089a3.086 3.086 0 004.351 0L15 20.442l8.164 8.162a3.086 3.086 0 004.352 0l1.089-1.09a3.088 3.088 0 000-4.351z",
        fill: "#fff",
        stroke: "#000"
      }
    )
  }
) });
export {
  e as default
};
