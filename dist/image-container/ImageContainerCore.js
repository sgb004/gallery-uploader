var x = Object.defineProperty;
var P = (r, t, s) => t in r ? x(r, t, { enumerable: !0, configurable: !0, writable: !0, value: s }) : r[t] = s;
var u = (r, t, s) => (P(r, typeof t != "symbol" ? t + "" : t, s), s), E = (r, t, s) => {
  if (!t.has(r))
    throw TypeError("Cannot " + s);
};
var e = (r, t, s) => (E(r, t, "read from private field"), s ? s.call(r) : t.get(r)), h = (r, t, s) => {
  if (t.has(r))
    throw TypeError("Cannot add the same private member more than once");
  t instanceof WeakSet ? t.add(r) : t.set(r, s);
}, d = (r, t, s, n) => (E(r, t, "write to private field"), n ? n.call(r, s) : t.set(r, s), s);
var l = (r, t, s) => (E(r, t, "access private method"), s);
var i, g, a, c, f, b, C, p, A, D, I, L, O, m, T;
class w {
  constructor(t, s, n, y = "data-position") {
    h(this, c);
    h(this, b);
    h(this, p);
    h(this, D);
    h(this, L);
    h(this, m);
    h(this, i, void 0);
    h(this, g, void 0);
    h(this, a, void 0);
    u(this, "exchangePosition");
    u(this, "positionAttribute");
    if (d(this, i, t), typeof s == "string") {
      const S = s;
      d(this, g, t.closest(S));
    } else
      d(this, g, s);
    d(this, a, {
      onDragStart: l(this, c, f).bind(this),
      onDragEnd: l(this, b, C).bind(this),
      onDragOver: l(this, p, A).bind(this),
      onDragEnter: l(this, D, I).bind(this),
      onDragLeave: l(this, L, O).bind(this),
      onDrop: l(this, m, T).bind(this)
    }), this.exchangePosition = n, this.positionAttribute = y, e(this, i).addEventListener("dragstart", e(this, a).onDragStart), e(this, i).addEventListener("dragend", e(this, a).onDragEnd), e(this, i).addEventListener("dragover", e(this, a).onDragOver), e(this, i).addEventListener("dragenter", e(this, a).onDragEnter), e(this, i).addEventListener("dragleave", e(this, a).onDragLeave), e(this, i).addEventListener("drop", e(this, a).onDrop);
  }
  destroy() {
    e(this, i).removeEventListener("dragstart", e(this, a).onDragStart), e(this, i).removeEventListener("dragend", e(this, a).onDragEnd), e(this, i).removeEventListener("dragover", e(this, a).onDragOver), e(this, i).removeEventListener("dragenter", e(this, a).onDragEnter), e(this, i).removeEventListener("dragleave", e(this, a).onDragLeave), e(this, i).removeEventListener("drop", e(this, a).onDrop);
  }
}
i = new WeakMap(), g = new WeakMap(), a = new WeakMap(), c = new WeakSet(), f = function(t) {
  const s = e(this, i).getAttribute(this.positionAttribute);
  e(this, i).classList.add("drag-start"), e(this, g).classList.add("drag-over"), t.dataTransfer && s && (t.dataTransfer.effectAllowed = "move", t.dataTransfer.setData("position", s));
}, b = new WeakSet(), C = function() {
  e(this, i).classList.remove("drag-start"), e(this, g).classList.remove("drag-over");
}, p = new WeakSet(), A = function(t) {
  return t.preventDefault(), !1;
}, D = new WeakSet(), I = function() {
  e(this, i).classList.add("drag-over");
}, L = new WeakSet(), O = function() {
  e(this, i).classList.remove("drag-over");
}, m = new WeakSet(), T = function(t) {
  if (t.stopPropagation(), t.target instanceof HTMLElement && t.dataTransfer) {
    const s = t.dataTransfer.getData("position"), n = t.target.getAttribute(this.positionAttribute);
    t.target.classList.remove("drag-over"), s != null && n != null && s !== n && this.exchangePosition(s, n);
  }
  return !1;
};
var o, v;
class N {
  constructor() {
    h(this, o, void 0);
    h(this, v, null);
    u(this, "observer");
    u(this, "exchangePosition", () => {
    });
    console.log("CONSTRUCTOR ImageContainerCore"), d(this, o, document.createElement("div")), this.observer = new MutationObserver(() => {
    }), this.observer.observe(e(this, o), {
      attributes: !0,
      attributeFilter: ["class"]
    });
  }
  destroy() {
    console.log("DESTRUYENDO ImageContainerCore"), this.removeListeners();
  }
  removeListeners() {
    e(this, v) && e(this, v).destroy(), this.observer.disconnect();
  }
  set imageContainer(t) {
    this.removeListeners();
    const s = () => {
      e(this, o).classList.contains("sending") ? this.disableInputs() : e(this, o).classList.contains("error") && e(this, o).classList.contains("upload") ? this.enableInputs(".delete, .download, .resend-upload") : this.enableInputs();
    };
    d(this, o, t), this.observer = new MutationObserver(s), this.observer.observe(e(this, o), {
      attributes: !0,
      attributeFilter: ["class"]
    }), d(this, v, new w(e(this, o), ".gallery-uploader", this.exchangePosition));
  }
  get imageContainer() {
    return e(this, o);
  }
  disableInputs(t = "input:not(.status), button:not(.status)") {
    const s = e(this, o).querySelectorAll(t);
    for (let n = 0; n < s.length; n++)
      s[n].setAttribute("disabled", "disabled");
  }
  enableInputs(t = "input:not(.status), button:not(.status)") {
    const s = e(this, o).querySelectorAll(t);
    for (let n = 0; n < s.length; n++)
      s[n].removeAttribute("disabled");
  }
}
o = new WeakMap(), v = new WeakMap();
export {
  N as default
};
