import { jsx as o } from "react/jsx-runtime";
const t = () => /* @__PURE__ */ o(
  "svg",
  {
    className: "icon",
    width: "30",
    height: "30",
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 30 30",
    children: /* @__PURE__ */ o(
      "path",
      {
        d: "M15 .5c-.806 0-1.611.483-1.611 1.45v16.796l-2.078-1.883c-1.526-1.371-3.813.685-2.288 2.057l4.832 4.348c.63.572 1.66.572 2.29 0l4.832-4.348c.634-.567.634-1.49 0-2.057-.63-.572-1.657-.572-2.288 0l-2.078 1.883V1.95C16.611.983 15.805.5 15 .5zM5.332 10.66C2.662 10.66.5 12.606.5 15.008v10.144c0 2.402 2.163 4.348 4.832 4.348h19.336c2.67 0 4.832-1.946 4.832-4.348V15.008c0-2.402-2.163-4.348-4.832-4.348h-3.225c-2.068.07-2.068 2.83 0 2.899h3.225c.89 0 1.61.648 1.61 1.449v10.144c0 .8-.72 1.449-1.61 1.45H5.332c-.89-.001-1.61-.65-1.61-1.45V15.008c0-.8.72-1.449 1.61-1.45h3.225c2.068-.069 2.068-2.829 0-2.898z",
        fill: "none",
        stroke: "#000",
        strokeLinecap: "round",
        strokeLinejoin: "round"
      }
    )
  }
), l = ({ onClick: c, url: n, fileName: e }) => n.startsWith("data:image/") ? /* @__PURE__ */ o("button", { className: "download button", onClick: c, children: /* @__PURE__ */ o(t, {}) }) : /* @__PURE__ */ o(
  "a",
  {
    href: n,
    download: e,
    className: "download button",
    target: "_blank",
    rel: "noreferrer",
    children: /* @__PURE__ */ o(t, {})
  }
);
export {
  l as default
};
