/// <reference types="react" />
export type ImageMessage = {
    type: 'error' | 'success' | 'warning' | 'none';
    subtype?: string;
    message: string;
};
export type ImageStatus = 'success' | 'warning' | 'error';
export type ImageSubStatus = '' | 'sending' | 'uploaded' | 'updated' | 'deleted' | 'upload' | 'update' | 'delete' | 'exchange' | 'exchanged' | 'get';
export type ImageNames = {
    name: string;
    fileName: string;
};
export type ImageInfo = {
    src: string;
    type?: string;
} & ImageNames;
export type ImageDataStatus = {
    status: ImageStatus;
    subStatus?: ImageSubStatus;
};
export type Image = {
    id: string;
    featured: boolean;
} & ImageInfo;
export type ImageData = Image & ImageDataStatus;
export type ImageSend = {
    id: string;
    type: 'upload' | 'update' | 'delete' | 'get' | 'exchange';
    status: {
        after: 'uploaded' | 'updated' | 'deleted' | 'get' | 'exchanged';
    };
    error: {
        type: 'upload' | 'update' | 'delete' | 'get' | 'exchange';
        message: string;
    };
    requestInit: 'uploadRequestInit' | 'updateRequestInit';
    url: string;
    body: object;
    afterFetch?: (value: any) => void;
    afterFetchError?: () => void;
    afterFetchComplete?: (value: any) => void;
};
export type ImageMessageList = {
    [key: string]: ImageMessage;
};
export type RequestInitType = 'upload' | 'update' | 'delete' | 'get' | 'exchange';
export type CustomRequestInit = undefined | ((type: RequestInitType) => RequestInit);
export type GalleryUploaderProps = {
    uploadUrl: string;
    updateUrl: string;
    deleteUrl: string;
    exchangePositionUrl: string;
    customRequestInit?: CustomRequestInit;
    images?: Image[];
};
export type ImageContainerProps = {
    id: string;
    src: string;
    name: string;
    fileName: string;
    featured: boolean;
    position: number;
    status: ImageStatus;
    subStatus: ImageSubStatus;
};
export type DeleteProps = {
    onDelete: React.MouseEventHandler<HTMLButtonElement>;
};
export type DownloadProps = {
    onClick: React.MouseEventHandler<HTMLButtonElement>;
    url: string;
    fileName?: string;
};
export type FeaturedProps = {
    featured: boolean;
    onChange: React.ChangeEventHandler<HTMLInputElement>;
};
export type InfoEditorProps = {
    id: string;
    info: ImageNames;
    setInfo: (imageNames: ImageNames) => void;
};
export type ResendUploadProps = {
    onClick: React.MouseEventHandler<HTMLButtonElement>;
};
export type UploadProps = {
    onChange: React.ChangeEventHandler<HTMLInputElement>;
};
