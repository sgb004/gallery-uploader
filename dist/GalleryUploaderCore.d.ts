/// <reference types="react" />
import { ImageMessage, ImageInfo, ImageData, ImageStatus, ImageSubStatus, ImageSend, ImageMessageList, CustomRequestInit } from './types';
declare class GalleryUploaderCore {
    #private;
    updateImages: (imagesData: ImageData[]) => void;
    imagesList: ImageData[];
    messagesList: ImageMessageList;
    sendUrls: {
        updateUrl: string;
        uploadUrl: string;
        deleteUrl: string;
        getUrl: string;
        exchangePositionUrl: string;
    };
    getCustomRequestInit: CustomRequestInit;
    constructor(imagesList?: ImageData[]);
    destroy(): void;
    removeListeners(): void;
    set galleryUploader(galleryUploader: HTMLElement);
    showMessage(id: string, message: ImageMessage): void;
    getPositionImage(id: string): number;
    getImageData(id: string): ImageData;
    changeStatusImage(id: string, status: ImageStatus, subStatus?: ImageSubStatus): void;
    reloadImages(): void;
    cleanName(fileName: string): string;
    handleFiles(files: FileList): void;
    getImageFromFile(file: File): Promise<ImageInfo>;
    getRequestInit(config: ImageSend): RequestInit;
    send(config: ImageSend): void;
    uploadImageChanged(id: string, file: File): void;
    uploadImage(imageData: ImageData): void;
    updateImage(id: string, name: string, fileName: string, featured: boolean, errorMessage: string, afterFetchError?: () => void, afterFetchComplete?: () => void): void;
    resendUpload(id: string): void;
    changeImageNames(id: string, name: string, fileName: string): void;
    exchangePosition(origin: string, destiny: string): void;
    onFileSelectorChange(event: React.ChangeEvent): void;
    onUploadImageChange(id: string, event: React.ChangeEvent): void;
    onFeaturedChange(id: string, event: React.ChangeEvent): void;
    onDelete(id: string): void;
    onStatusClick(id: string): void;
    onDownload(id: string): void;
}
export default GalleryUploaderCore;
