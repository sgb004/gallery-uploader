import "./assets/GalleryUploader.css";
import { jsx as u, jsxs as c } from "react/jsx-runtime";
import { useRef as S, useState as y, useMemo as b, useEffect as n } from "react";
import h from "./GalleryUploaderCore.js";
import L from "./GalleryUploaderContext.js";
import N from "./image-container/ImageContainer.js";
const w = ({
  uploadUrl: i,
  updateUrl: d,
  deleteUrl: f,
  exchangePositionUrl: p,
  customRequestInit: l,
  images: a = []
}) => {
  const o = S(null);
  let [g, m] = y([]), [s, C] = y(0), t = b(() => new h(), []);
  return n(() => {
    const e = () => {
      s = s + 1, C(s);
    };
    t.updateImages = (r) => {
      m(r), e();
    };
  }, [t, s]), n(() => {
    if (a.length > 0) {
      const e = a.map((r) => ({ ...r, status: "success", subStatus: "uploaded" }));
      t.imagesList = e;
    }
  }, [t, a]), n(() => {
    let e = () => {
    };
    return o.current && (t.galleryUploader = o.current, t.sendUrls = {
      uploadUrl: i,
      updateUrl: d,
      deleteUrl: f,
      getUrl: "",
      exchangePositionUrl: p
    }, l && (t.getCustomRequestInit = l), m(t.imagesList), e = t.destroy.bind(t)), e;
  }, [t, i, d, f, p, l]), /* @__PURE__ */ u(L.Provider, { value: { core: t }, children: /* @__PURE__ */ c("div", { ref: o, className: "gallery-uploader", children: [
    g.map((e, r) => /* @__PURE__ */ u(
      N,
      {
        id: e.id,
        src: e.src,
        name: e.name,
        fileName: e.fileName,
        featured: e.featured,
        position: r,
        status: e.status,
        subStatus: e.subStatus ?? ""
      },
      r
    )),
    /* @__PURE__ */ c("label", { className: "file-selector", children: [
      "Select file",
      /* @__PURE__ */ u(
        "input",
        {
          type: "file",
          onChange: (e) => {
            t.onFileSelectorChange(e);
          }
        }
      )
    ] })
  ] }) });
};
export {
  w as default
};
