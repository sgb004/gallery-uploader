/// <reference types="react" />
import GalleryUploaderCore from './GalleryUploaderCore';
declare const GalleryUploaderContext: import("react").Context<{
    core: GalleryUploaderCore;
}>;
export default GalleryUploaderContext;
