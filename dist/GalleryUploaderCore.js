var L = Object.defineProperty;
var b = (i, e, t) => e in i ? L(i, e, { enumerable: !0, configurable: !0, writable: !0, value: t }) : i[e] = t;
var d = (i, e, t) => (b(i, typeof e != "symbol" ? e + "" : e, t), t), I = (i, e, t) => {
  if (!e.has(i))
    throw TypeError("Cannot " + t);
};
var m = (i, e, t) => (I(i, e, "read from private field"), t ? t.call(i) : e.get(i)), y = (i, e, t) => {
  if (e.has(i))
    throw TypeError("Cannot add the same private member more than once");
  e instanceof WeakSet ? e.add(i) : e.set(i, t);
}, c = (i, e, t, s) => (I(i, e, "write to private field"), s ? s.call(i, t) : e.set(i, t), t);
var h;
class w {
  constructor(e) {
    y(this, h, void 0);
    d(this, "updateImages");
    d(this, "imagesList", []);
    d(this, "messagesList", {});
    d(this, "sendUrls", { updateUrl: "", uploadUrl: "", deleteUrl: "", getUrl: "", exchangePositionUrl: "" });
    d(this, "getCustomRequestInit");
    console.log("CONSTRUCTOR GalleryUploaderCore"), e && (this.imagesList = e), c(this, h, document.createElement("div")), this.updateImages = () => {
    };
  }
  destroy() {
    console.log("DESTRUCTOR GalleryUploaderCore"), this.removeListeners();
  }
  removeListeners() {
  }
  set galleryUploader(e) {
    this.removeListeners(), c(this, h, e);
  }
  showMessage(e, t) {
    this.messagesList[e] = t, t.type === "success" ? console.log(t.message) : t.type === "error" ? console.error(t.message) : console.warn(t.message);
  }
  getPositionImage(e) {
    return this.imagesList.findIndex((t) => t.id === e);
  }
  getImageData(e) {
    const t = this.getPositionImage(e);
    return this.imagesList[t];
  }
  changeStatusImage(e, t, s) {
    const a = this.getPositionImage(e);
    this.imagesList[a].status = t, this.imagesList[a].subStatus = s, this.reloadImages();
  }
  reloadImages() {
    this.updateImages(this.imagesList);
  }
  cleanName(e) {
    let t = e;
    return t = t.replace(/[\-\_]/gm, " "), t = t.replace(/\.[^/.]+$/, ""), t.trim();
  }
  handleFiles(e) {
    let t = [];
    for (let s = 0; s < e.length; s++)
      t.push(this.getImageFromFile(e[s]));
    Promise.all(t).then((s) => {
      for (const a of s) {
        const o = {
          id: `${(/* @__PURE__ */ new Date()).getTime()}`,
          status: "success",
          featured: !1,
          ...a
        };
        this.imagesList.push(o), this.reloadImages(), this.uploadImage(o);
      }
    }).catch(
      (s) => this.showMessage("none", {
        type: "error",
        message: s
      })
    );
  }
  getImageFromFile(e) {
    return new Promise((t, s) => {
      if (e.type.startsWith("image/")) {
        const a = new FileReader();
        a.onload = (r) => {
          r.target && typeof r.target.result == "string" && t({
            src: r.target.result,
            name: this.cleanName(e.name),
            fileName: e.name
          });
        }, a.readAsDataURL(e);
      } else
        s(`The file "${e.name}" is not an image.`);
    });
  }
  getRequestInit(e) {
    let t = {
      "Content-Type": "application/json",
      Accept: "application/json, text-plain, */*"
    }, s = e.body;
    const a = this.getCustomRequestInit ? this.getCustomRequestInit(e.type) : {};
    return this.changeStatusImage(e.id, "warning", "sending"), t = typeof a.headers == "object" ? { ...a.headers, ...t } : t, s = typeof a.body == "object" ? { ...a.body, ...s } : s, s.id = e.id, a.headers = t, a.method = "POST", a.body = JSON.stringify(s), a;
  }
  send(e) {
    const t = this.getRequestInit(e);
    fetch(e.url, t).then((s) => s.json()).then((s) => {
      e.afterFetch && e.afterFetch(s), s.success === !0 ? (this.changeStatusImage(e.id, "success", e.status.after), this.showMessage(e.id, {
        type: "success",
        subtype: e.status.after,
        message: s.message
      })) : (this.showMessage(e.id, {
        type: "error",
        subtype: e.error.type,
        message: s.message
      }), this.changeStatusImage(e.id, "error", e.error.type)), e.afterFetchComplete && e.afterFetchComplete(s);
    }).catch((s) => {
      throw this.showMessage(e.id, {
        type: "error",
        subtype: e.error.type,
        message: e.error.message
      }), this.changeStatusImage(e.id, "error", e.error.type), e.afterFetchError && e.afterFetchError(), new Error(s);
    });
  }
  uploadImageChanged(e, t) {
    this.getImageFromFile(t).then((s) => {
      const a = this.getPositionImage(e), r = {
        id: `${e}`,
        status: "success",
        featured: this.imagesList[a].featured,
        ...s
      };
      this.imagesList[a].src = s.src, this.uploadImage(r);
    }).catch(
      (s) => this.showMessage(e, {
        type: "error",
        subtype: "",
        message: s
      })
    );
  }
  uploadImage(e) {
    this.send({
      id: e.id,
      type: "upload",
      status: {
        after: "uploaded"
      },
      error: {
        type: "upload",
        message: "Error to upload."
      },
      requestInit: "uploadRequestInit",
      url: this.sendUrls.uploadUrl,
      body: e
    });
  }
  updateImage(e, t, s, a, r, o, n) {
    this.send({
      id: e,
      type: "update",
      status: {
        after: "updated"
      },
      error: {
        type: "update",
        message: r
      },
      requestInit: "updateRequestInit",
      url: this.sendUrls.updateUrl,
      body: {
        name: t,
        fileName: s,
        featured: a
      },
      afterFetch: (g) => {
        g.success !== !0 && o && o();
      },
      afterFetchError: o,
      afterFetchComplete: n
    });
  }
  resendUpload(e) {
    this.uploadImage(this.getImageData(e));
  }
  changeImageNames(e, t, s) {
    const a = this.getPositionImage(e);
    this.updateImage(
      e,
      t,
      s,
      this.imagesList[a].featured,
      "Error to change the info of the image.",
      () => {
      },
      () => {
        this.imagesList[a].name = t, this.imagesList[a].fileName = s, this.reloadImages();
      }
    );
  }
  exchangePosition(e, t) {
    const s = parseInt(e), a = parseInt(t), r = this.imagesList[s].id, o = this.imagesList[a].id, n = this.imagesList[a];
    this.imagesList[a] = this.imagesList[s], this.imagesList[s] = n, this.send({
      id: r,
      type: "exchange",
      status: {
        after: "exchanged"
      },
      error: {
        type: "exchange",
        message: "Error to change position"
      },
      requestInit: "updateRequestInit",
      url: this.sendUrls.exchangePositionUrl,
      body: {
        origin: e,
        destiny: t,
        originId: r,
        destinyId: o
      }
    });
  }
  onFileSelectorChange(e) {
    const t = e.target;
    t instanceof HTMLInputElement && t.files && (this.handleFiles(t.files), t.value = "");
  }
  onUploadImageChange(e, t) {
    const s = t.target;
    s instanceof HTMLInputElement && s.files && (this.uploadImageChanged(e, s.files[0]), s.value = "");
  }
  onFeaturedChange(e, t) {
    const s = t.target;
    if (s instanceof HTMLInputElement) {
      const a = this.getPositionImage(e), r = s.checked, o = this.imagesList[a].featured, n = () => {
        this.imagesList[a].featured = o, s.checked = o;
      };
      this.imagesList[a].featured = r, this.updateImage(
        e,
        this.imagesList[a].name,
        this.imagesList[a].fileName,
        r,
        "Error to change the value of featured of the image.",
        n
      );
    }
  }
  onDelete(e) {
    window.confirm(
      "Do you want to delete the image? This action cannot be undone."
    ) && this.send({
      id: e,
      type: "delete",
      status: {
        after: "deleted"
      },
      error: {
        type: "delete",
        message: "Error to delete the image."
      },
      requestInit: "updateRequestInit",
      url: this.sendUrls.deleteUrl,
      body: {},
      afterFetchComplete: (s) => {
        s.success === !0 && (this.imagesList = this.imagesList.filter(
          (a) => a.id !== e
        ), this.reloadImages());
      }
    });
  }
  onStatusClick(e) {
    this.messagesList[e] && this.showMessage(e, this.messagesList[e]);
  }
  onDownload(e) {
    const t = this.getImageData(e), [s, a] = t.src.split(","), r = s.replace(/(data:)|(;base64)/g, ""), o = atob(a), n = new ArrayBuffer(o.length), g = new Uint8Array(n);
    for (let u = 0; u < o.length; u++)
      g[u] = o.charCodeAt(u);
    const f = new Blob([g], { type: r }), p = window.URL.createObjectURL(f), l = document.createElement("a");
    l.style.display = "none", l.href = p, l.download = t.fileName, m(this, h).appendChild(l), l.click(), window.URL.revokeObjectURL(p), m(this, h).removeChild(l);
  }
}
h = new WeakMap();
export {
  w as default
};
