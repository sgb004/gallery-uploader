import express from 'express';
import cors from 'cors';

const app = new express();
const PORT = 9033;

const corsOptions = {
	origin: ['http://localhost:3000', 'http://localhost:9000', 'http://localhost:5173'],
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
	preflightContinue: false,
	optionsSuccessStatus: 204,
};

app.use(cors(corsOptions));
app.use(express.json({ limit: '500mb' }));
app.use(express.urlencoded({ limit: '500mb', extended: true }));

app.listen(PORT, () => {
	console.log('SERVER LISTENING on PORT', PORT);
});

app.post('/upload-error', (request, response) => {
	const status = { success: false, message: 'Error to store image.' };

	console.log('/upload-error');
	console.log(request.body);

	response.send(status);
});

app.post('/upload-success', (request, response) => {
	const status = { success: true, message: 'Image update.' };

	console.log('/upload-success');
	console.log(request.body);

	response.send(status);
});

app.post('/update-error', (request, response) => {
	const status = { success: false, message: 'Error to update image.' };

	console.log('/update-error');
	console.log(request.body);

	response.send(status);
});

app.post('/update-success', (request, response) => {
	const status = { success: true, message: 'Image updated.' };

	console.log('/update-success');
	console.log(request.body);

	response.send(status);
});

app.post('/exchange-position-error', (request, response) => {
	const status = { success: false, message: 'Error to delete image.' };

	console.log('/delete-error');
	console.log(request.body);

	response.send(status);
});

app.post('/delete-success', (request, response) => {
	const status = { success: true, message: 'Image deleted.' };

	console.log('/delete-success');
	console.log(request.body);

	response.send(status);
});

app.post('/exchange-position-error', (request, response) => {
	const status = { success: false, message: 'Error to change position.' };

	console.log('/exchange-position-error');
	console.log(request.body);

	response.send(status);
});

app.post('/exchange-position-success', (request, response) => {
	const status = { success: true, message: 'The images were changed of position.' };

	console.log('/exchange-position-success');
	console.log(request.body);

	response.send(status);
});
